public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo đối tượng account 1 k có balance
        Account account1 = new Account(1, "Jack");
        // khởi tạo đối tượng account 2 có balance
        Account account2 = new Account(1, "Rose", 1000);

        System.out.println("Account 1");
        System.out.println(account1.toString());
        System.out.println("Account 2");
        System.out.println(account2.toString());

        //Tăng balance
        account1.credit(2000);
        account2.credit(3000);

        System.out.println("Account 1 - raise amount");
        System.out.println(account1.toString());
        System.out.println("Account 2 - raise amount");
        System.out.println(account2.toString());

        //giảm tài khoản
        account1.debit(1000);
        account2.debit(5000);

        System.out.println("Account 1 - subtract amount");
        System.out.println(account1.toString());
        System.out.println("Account 2 - subtract amount");
        System.out.println(account2.toString());

        //transfer
        account1.tranferTo(account2, 2000);
        account2.tranferTo(account1, 2000);

        System.out.println("Account 1 - transfer amount");
        System.out.println(account1.toString());
        System.out.println("Account 2 - transfer amount");
        System.out.println(account2.toString());




    }
}
