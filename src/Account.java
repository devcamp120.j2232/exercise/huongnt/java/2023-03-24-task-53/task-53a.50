public class Account {
    // khai báo thuộc tính
    public int id;
    public String name;
    public int balance = 0;

    // phương thức khởi tạo
    public Account(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Account(int id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    // getter
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    // các phương thức khác

    public int credit(int amount) {
        this.balance += amount;
        return this.balance;
    }

    public int debit(int amount) {
        if (amount <= this.balance) {
            this.balance = balance - amount;
        } else {
            System.out.println("Amount execeeded balance");
        }
        return this.balance;
    }

    public int tranferTo(Account Account, int amount) {
        if (amount <= this.balance) {
            Account.balance += amount;
            this.balance = this.balance - amount;
        } else {
            System.out.println("Amount execeeded balance");
        }
        return this.balance;
    }

    // in ra console log
    @Override
    public String toString() {
        return String.format("Account [id = %s, name = %s, balance = %s]", id, name, balance);
    }
}
